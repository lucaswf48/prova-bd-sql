-- Listar nome, marca e tipo de todas as impressoras existentes

SELECT nome,marca,tipo FROM impressoras;

-- Listar o nome, cargo, e salário de cada funcionário

SELECT pessoas.nome,cargo.nome,cargo.salario FROM pessoas
JOIN funcionarios ON pessoas.id = funcionarios.pessoas_id
JOIN cargo ON cargo.id = funcionarios.cargo_id;

-- Listar as cidades dos clientes onde houve atendimento

SELECT endereco.cidade FROM endereco
JOIN pessoas ON endereco.pessoas_id = pessoas.id
JOIN atendimento ON atendimento.clientes_id = pessoas.id
GROUP BY endereco.cidade;

-- Listar os detalhes de todos os atendimentos presentes

SELECT * FROM atendimento;

-- Listar o nome dos clientes que foram atendidos em dezembro de 2015
SELECT pessoas.nome,atendimento.data FROM pessoas
JOIN atendimento ON pessoas.id = atendimento.clientes_id
WHERE EXTRACT(MONTH FROM atendimento.data) = 12 AND EXTRACT(YEAR FROM atendimento.data) = 2015
GROUP BY pessoas.nome;

-- Listar a média de ganhos dos funcionários
SELECT AVG(cargo.salario) FROM cargo;

-- Listar quantas cópias cada impressora imprimiu no último ano
SELECT SUM(atendimento.num_paginas) FROM atendimento
JOIN impressoras ON impressoras.id = atendimento.impressoras_id
GROUP BY impressoras.id
ORDER BY SUM(atendimento.num_paginas);

-- Listar o faturamento anual da empresa (anos mais recentes primeiro)

SELECT SUM(atendimento.valor),EXTRACT(YEAR FROM atendimento.data) FROM atendimento
GROUP BY EXTRACT(YEAR FROM atendimento.data)
ORDER BY EXTRACT(YEAR FROM atendimento.data) DESC;

-- Listar o faturamento mensal da empresa (meses mais recente primeiro)

SELECT SUM(atendimento.valor),EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data) FROM atendimento
GROUP BY EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data)
ORDER BY EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data) DESC;

-- Listar os funcionários mais lucrativos (aqueles que venderam mais nos atendimentos)

SELECT pessoas.nome, SUM(atendimento.valor) FROM atendimento
JOIN funcionarios ON funcionarios.pessoas_id = atendimento.funcionarios_id
JOIN pessoas ON funcionarios.pessoas_id = pessoas.id
GROUP BY atendimento.funcionarios_id
ORDER BY SUM(atendimento.valor) DESC;

-- Listar a quantidade de atendimentos realizados por funcionário, por mês, em ordem descrescente.
SELECT pessoas.nome,count(atendimento.funcionarios_id),EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data) FROM atendimento
JOIN funcionarios ON funcionarios.pessoas_id = atendimento.funcionarios_id
JOIN pessoas ON funcionarios.pessoas_id = pessoas.id
GROUP BY atendimento.funcionarios_id,EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data)
ORDER BY count(atendimento.funcionarios_id) DESC;

-- Listar o funcionário do mês -- o quanto vendeu e quantos antedimentos fez -- de cada mês do último ano (o funcionário do mês é aquele que mais vendou no período).

-- Maior lucro e não funciona

SELECT pessoas.nome,SUM(atendimento.valor),COUNT(atendimento.funcionarios_id),EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data) FROM atendimento
JOIN funcionarios ON funcionarios.pessoas_id = atendimento.funcionarios_id
JOIN pessoas ON funcionarios.pessoas_id = pessoas.id
GROUP BY atendimento.funcionarios_id,EXTRACT(MONTH FROM atendimento.data),EXTRACT(YEAR FROM atendimento.data)
ORDER BY EXTRACT(YEAR FROM atendimento.data) DESC,EXTRACT(MONTH FROM atendimento.data),SUM(atendimento.valor)
LIMIT 12;

